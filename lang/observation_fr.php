<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
return [
	// A
	'contrib_rubrique_plugin_rubplug_loc'    => 'Rubrique mal placée',
	'contrib_rubrique_plugin_rubplug_pfx'    => 'Préfixe erroné',
	'contrib_rubrique_categorie_rubcat1_abs' => 'Rubrique absente',
	'contrib_rubrique_categorie_rubcat1_max' => 'Rubriques dupliquées (@rubriques@)',
	'contrib_rubrique_categorie_rubcat0_abs' => 'Secteur absent',
	'contrib_rubrique_categorie_rubcat0_max' => 'Secteurs dupliqués (@rubriques@)',
	'contrib_plugin_affectation_plugpfx_nok' => 'Préfixe invalide'
];
