<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insertion dans le pipeline declarer_tables_objets_sql (SPIP).
 *
 * Declarer les champs categorie et préfixe pour les rubriques.
 *
 * @param null|array $champs La définition des objets SPIP
 *
 * @return array La définition des objets SPIP modifiés
 */
function galactic_contrib_declarer_champs_extras(?array $champs = []) {
	// Table : spip_documents
	$champs['spip_documents']['etat'] = [
		'saisie'  => 'selection',
		'options' => [
			'nom'          => 'etat',
			'label'        => 'État du plugin',
			'restrictions' => [
				'modifier' => [
					'auteur' => 'admin',
				],
			],
			'data' => [
				'stable'       => '<:ecrire:plugin_etat_stable:>',
				'test'         => '<:ecrire:plugin_etat_test:>',
				'dev'          => '<:ecrire:plugin_etat_developpement:>',
				'experimental' => '<:ecrire:plugin_etat_experimental:>'
			],
			'versionner'             => true,
			'sql'                    => 'varchar(12) DEFAULT \'\' NOT NULL',
			'rechercher_ponderation' => '2',
		],
	];

	// Table : spip_rubriques, on initialise les champs extras de la table.
	// Ajout de la catégorie de plugin. La saisie est une sélection particulière.
	$champs['spip_rubriques']['categorie'] = [
		'saisie'  => 'rubrique_categorie',
		'options' => [
			'nom'          => 'categorie',
			'label'        => '<:svptype:categorie_identifiant_label:>',
			'option_intro' => '<:galactic_contrib:categorie_vide_label:>',
			'env'          => true,
			'restrictions' => [
				'modifier' => [
					'auteur' => 'webmestre',
				],
				'voir' => false,
			],
			'sql'                    => "varchar(100) DEFAULT '' NOT NULL",
			'rechercher'             => true,
			'rechercher_ponderation' => 2,
			'versionner'             => false,
		],
		'verifier' => [
		],
	];

	// Ajout du préfixe de plugin. La saisie est un input simple.
	$champs['spip_rubriques']['prefixe'] = [
		'saisie'  => 'input',
		'options' => [
			'nom'          => 'prefixe',
			'label'        => '<:svp:label_prefixe:>',
			'restrictions' => [
				'modifier' => [
					'auteur' => 'webmestre',
				],
				'voir' => false,
			],
			'sql'                    => "varchar(30) DEFAULT '' NOT NULL",
			'rechercher'             => true,
			'rechercher_ponderation' => 10,
			'versionner'             => false,
		],
		'verifier' => [
		],
	];

	// Table : spip_rubriques, on initialise les champs extras de la table.
	// Ajout de la catégorie de plugin. La saisie est une sélection particulière.
	$champs['spip_articles']['type_article'] = [
		'saisie'  => 'article_type',
		'options' => [
			'nom'   => 'type_article',
			'label' => '<:galactic_contrib:type_article_label:>',
			'data'  => [
				''           => '<:galactic_contrib:type_article_utilisation_label:>',
				'conception' => '<:galactic_contrib:type_article_conception_label:>',
				'actualite'  => '<:galactic_contrib:type_article_actualite_label:>'
			],
			'exclusions' => [
				''           => [],
				'conception' => ['apropos'],
				'actualite'  => ['apropos', 'carnet']
			],
			'env'          => true,
			'restrictions' => [
				'modifier' => [
					'auteur' => 'webmestre',
				],
			],
			'sql'        => "varchar(16) DEFAULT '' NOT NULL",
			'versionner' => false,
		],
		'verifier' => [
		],
	];

	return $champs;
}
