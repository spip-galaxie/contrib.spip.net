<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/contrib.spip.net.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	// M
	'motif_archive_article_obsolescence_label'     => 'article archivé car obsolescent',
	'motif_archive_rubrique_obsolescence_label'    => 'rubrique archivée car obsolescente',
	'motif_desarchive_article_obsolescence_label'  => 'article désarchivé suite à une erreur d\'archivage',
	'motif_desarchive_rubrique_obsolescence_label' => 'rubrique désarchivée suite à une erreur d\'archivage',
];
