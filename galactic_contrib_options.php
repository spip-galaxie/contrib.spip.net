<?php

// Quelques reglages d'affichage
$GLOBALS['puce'] = '- ';

define('_BOUCLE_PROFILER', 5000);
define('_CALCUL_PROFILER', 100);

// Boussole SPIP présente
define('_SPIP_TOPNAV', true);

if (strncmp((string) _request('recherche'), 'http', 4) == 0 and _request('page') != 'recherche') {
	$ecran_securite_raison = 'Recherche mal formee';
	if ($GLOBALS['ip'] and date('s') == 0) {
		touch(_DIR_RACINE . _NOM_TEMPORAIRES_INACCESSIBLES . 'flood/' . $GLOBALS['ip']);
	}
}
if (isset($ecran_securite_raison)) {
	header('HTTP/1.0 403 Forbidden');
	header('Expires: Wed, 11 Jan 1984 05:00:00 GMT');
	header('Cache-Control: no-cache, must-revalidate');
	header('Pragma: no-cache');
	header('Content-Type: text/html');
	die("<html lang='fr'><title>Error 403: Forbidden</title><body><h1>Error 403</h1><p>You are not authorized to view this page ({$ecran_securite_raison})</p></body></html>");
}

//if (is_dir($f='/dev/shm/cache-contribspipnet')) define('_DIR_CACHE',"$f/");
define('_UNIVERS_STATSV_FILE', '../IMG/spip-histoversion-stats-192030.json');

// si c'est une page forum ou une vieille page :
// un GET est redirige sur l'article de l'id_article ou sur la home
// un POST est refuse en 403
if ($p = _request('page') and $p === 'forum') {
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$raison = 'Acces interdit';
		header('HTTP/1.0 403 Forbidden');
		header('Expires: Wed, 11 Jan 1984 05:00:00 GMT');
		header('Cache-Control: no-cache, must-revalidate');
		header('Pragma: no-cache');
		header('Content-Type: text/html');
		die("<html lang='fr'><title>Error 403: Forbidden</title><body><h1>Error 403</h1><p>You are not authorized to view this page ({$raison})</p></body></html>");
	} else {
		$url = 'https://contrib.spip.net/';
		if ($id_article = (int) (_request('id_article'))) {
			$url .= "?article{$id_article}";
		}
		include_spip('inc/headers');
		spip_initialisation_core(
			(_DIR_RACINE . _NOM_PERMANENTS_INACCESSIBLES),
			(_DIR_RACINE . _NOM_PERMANENTS_ACCESSIBLES),
			(_DIR_RACINE . _NOM_TEMPORAIRES_INACCESSIBLES),
			(_DIR_RACINE . _NOM_TEMPORAIRES_ACCESSIBLES)
		);
		redirige_par_entete($url, '', 301);
	}
}

// Prefixe et chemin des cookies
// (a modifier pour installer des sites SPIP dans des sous-repertoires)
//$GLOBALS['cookie_prefix'] = "contrib";
//$GLOBALS['cookie_path'] = "";

// Recherche fulltext sur des mots partiels
defined('_FULLTEXT_ASTERISQUE_PARTOUT') || define('_FULLTEXT_ASTERISQUE_PARTOUT', true);

//##################
// parametrages pour "Autorité"
//##################
// defini les "webmestres" au sens de Autorite
define('_ID_WEBMESTRES', '1:4:198:589:989:5384:6809:2650:5645:6502:4316');

//#################
// parametrage des URLs
//##################
$GLOBALS['type_urls'] = 'propres';
define('_debut_urls_propres', '');
define('_MARQUEUR_URL', '');
define('_URLS_PROPRES_MAX', 55);

//#################
// parametrage des paginations de commentaires
// (si double @@, retrouver l’id_thread)
//##################
if ($i = _request('debut_comments-list')
	and strncmp($i, '@@', 2) == 0
	and $id_forum = (int) (substr($i, 2))) {
	@spip_initialisation_core(
		(_DIR_RACINE . _NOM_PERMANENTS_INACCESSIBLES),
		(_DIR_RACINE . _NOM_PERMANENTS_ACCESSIBLES),
		(_DIR_RACINE . _NOM_TEMPORAIRES_INACCESSIBLES),
		(_DIR_RACINE . _NOM_TEMPORAIRES_ACCESSIBLES)
	);

	include_spip('base/abstract_sql');
	if ($id_thread = sql_getfetsel('id_thread', 'spip_forum', 'id_forum=' . (int) $id_forum . " AND statut='publie'")) {
		// on place la pagination indirecte sur le thread, au lieu du forum
		set_request('debut_comments-list', "@{$id_thread}");
	} else {
		// sinon au debut de la liste paginee
		set_request('debut_comments-list', '0');
	}
}

// On force le mode d'utilisation de SVP a non runtime car on veut presenter tous les
// plugins contenus dans les depots quelque soit leur compatibilite spip
if (!defined('_SVP_MODE_RUNTIME')) {
	define('_SVP_MODE_RUNTIME', false);
}

// Liste des pages publiques d'objet supportees par le squelette (depot, plugin).
// Permet d'afficher le bouton voir en ligne dans la page d'edition de l'objet
if (!defined('_SVP_PAGES_OBJET_PUBLIQUES')) {
	define('_SVP_PAGES_OBJET_PUBLIQUES', 'depot:plugin');
}

// Taille des listes et pas de pagination de la page sommaire
if (!defined('_PLUGINSPIP_TAILLE_SELECTION_PLUGINS')) {
	define('_PLUGINSPIP_TAILLE_SELECTION_PLUGINS', 10);
}
if (!defined('_PLUGINSPIP_TAILLE_TOP_PLUGINS')) {
	define('_PLUGINSPIP_TAILLE_TOP_PLUGINS', 30);
}
if (!defined('_PLUGINSPIP_TAILLE_MAJ_PLUGINS')) {
	define('_PLUGINSPIP_TAILLE_MAJ_PLUGINS', 30);
}
if (!defined('_PLUGINSPIP_PAS_TOP_PLUGINS')) {
	define('_PLUGINSPIP_PAS_TOP_PLUGINS', 5);
}
if (!defined('_PLUGINSPIP_PAS_MAJ_PLUGINS')) {
	define('_PLUGINSPIP_PAS_MAJ_PLUGINS', 5);
}
if (!defined('_SVP_PERIODE_ACTUALISATION_DEPOTS')) {
	define('_SVP_PERIODE_ACTUALISATION_DEPOTS', 1);
}

// Branche SPIP stable
if (!defined('_PLUGINSPIP_BRANCHE_STABLE')) {
	define('_PLUGINSPIP_BRANCHE_STABLE', '4.2');
}

// Branches SPIP maintenues
if (!defined('_PLUGINSPIP_BRANCHES_MAINTENUES')) {
	define('_PLUGINSPIP_BRANCHES_MAINTENUES', '4.2,4.1,4.0,3.2');
}

// Période de rafaichissement du cache autodoc
if (!defined('_PLUGINSPIP_TIMEOUT_AUTODOC')) {
	define('_PLUGINSPIP_TIMEOUT_AUTODOC', 3600 * 24);
}

// Forcer l'utilisation de la langue du visiteur
$GLOBALS['forcer_lang'] = true;

include_spip('inc/contrib_rubrique');
