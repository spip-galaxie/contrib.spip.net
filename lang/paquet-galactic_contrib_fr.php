<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/contrib.spip.net.git

return [

	// G
	'galactic_contrib_description' => 'Le plugin permet de mettre en place le look de Contrib basé sur le squelette générique Galactic et de créer une structure et des mécanismes d’échanges avec les contributeurs de façon à organiser et faciliter leurs contributions. Il propose aussi aux administrateurs une interface de contrôle pour assurer la pérennité de la structure mise en place.',
	'galactic_contrib_nom' => 'Contrib - Squelette & Maintenance',
	'galactic_contrib_slogan' => 'Look galactique & maintenance de SPIP-Contrib',
];
