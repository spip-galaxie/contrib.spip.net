<?php
/**
 * Gestion du formulaire de configuration du plugin.
 *
 * @package SPIP\TAXONOMIE\CONFIGURATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire propose la liste des secteurs disponibles (hors apropos, carnet et plugin).
 * L'utilisateur doit choisir le ou les secteurs qui corresponderont à la partie Galaxie du site.
 *
 * @return array Tableau des données à charger par le formulaire (affichage ou données de configuration).
 *               - `_secteur_possibles` : (affichage) liste des secteurs disponibles (hors apropos, carnet et plugin).
 *               - `secteurs`           : (configuration) liste des secteurs galaxie choisis.
 */
function formulaires_configurer_galactic_contrib_charger() : array {
	// Chargement des données de configuration déjà en meta
	include_spip('inc/cvt_configurer');
	$valeurs = cvtconf_formulaires_configurer_recense('configurer_galactic_contrib');

	// Liste des secteurs carnet wiki et archives (vieux articles non visibles exclus par exclure_sect) :
	// -- les exclure car il ne peuvent pas être choisis.
	include_spip('inc/config');
	$exclusions = lire_config('secteur/exclure_sect', []);
	$exclusions = array_merge($exclusions, lire_config('autorite/espace_wiki', []));

	// Sélection des secteurs pouvant être choisis pour la galaxie en excluant aussi les secteurs-plugin qui
	// sont ceux qui ont déjà une catégorie non vide.
	$from = 'spip_rubriques';
	$where = [
		'profondeur=0',
		'categorie=' . sql_quote(''),
		sql_in('id_rubrique', $exclusions, 'NOT')
	];
	$secteurs = sql_allfetsel('id_rubrique, titre', $from, $where);
	$valeurs['_secteur_possibles'] = array_column($secteurs, 'titre', 'id_rubrique');
	foreach ($valeurs['_secteur_possibles'] as $_id => $_titre) {
		$valeurs['_secteur_possibles'][$_id] = typo($_titre);
	}

	$valeurs['editable'] = true;

	return $valeurs;
}

/**
 * Vérification des saisies : les secteurs aide et galaxie doivent être disjoints.
 *
 * @return array Tableau des erreurs : seul l'index du secteur galaxie est utilisé pour renvoyer l'erreur d'intersection
 */
function formulaires_configurer_galactic_contrib_verifier() : array {
	// Initialisation des erreurs de saisie
	$erreurs = [];

	// On vérifie que les secteurs sont disjoints
	$aide = _request('secteur_aide') ?? 0;
	$galaxie = _request('secteur_galaxie') ?? 0;

	if (!$aide) {
		$erreurs['secteur_aide'] = _T('info_obligatoire');
	}

	if (!$galaxie) {
		$erreurs['secteur_galaxie'] = _T('info_obligatoire');
	} elseif ($aide === $galaxie) {
		$erreurs['secteur_galaxie'] = _T('galactic_contrib:configuration_secteur_galaxie_erreur');
	}

	return $erreurs;
}
