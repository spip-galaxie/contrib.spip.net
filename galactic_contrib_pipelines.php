<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Afficher les prolégomènes aux rapports de bug sur les formulaires de forum.
 *
 * @param array $flux Flux d'entrée
 *
 * @return array Flux mis à jour
 **/
function galactic_contrib_formulaire_fond(array $flux) : array {
	if (
		$flux['args']['form'] === 'forum'
		and $flux['args']['je_suis_poste'] === false
	) {
		$texte = &$flux['data'];
		if (strpos($texte, '<textarea name="texte"')) {
			$ainserer = '<div class="explication">' . propre(sql_getfetsel('texte', 'spip_articles', 'id_article=3597')) . '</div>';
			$texte = $ainserer . $texte;
		}
	}

	return $flux;
}

/**
 * Surcharge de la fonction charger des formulaires concernés, a savoir :
 * - editer_article_accueil.
 *
 * @param array $flux Flux d'entrée
 *
 * @return array Flux mis à jour
 **/
function galactic_contrib_formulaire_charger(array $flux) : array {
	// Personnalisation du formulaire de choix de l'article d'accueil
	if ($flux['args']['form'] === 'editer_article_accueil') {
		// Choisir la liste des statuts autorisés, le filtre sur le type d'article
		// et le titre du bloc
		$flux['data']['_statuts'] = ['prepa', 'prop', 'publie'];
		$flux['data']['_where'] = "type_article=''";
		$flux['data']['_titre'] = _T('galactic_contrib:article_accueil_titre');
	}

	return $flux;
}

/**
 * Ajouter du contenu dans la colonne de droite (extra).
 *
 * Page d'un mot "type de plugin" représentant une catégorie de niveau 0  :
 * - ajout du formulaire de choix de la couleur de la rubrique
 *
 * @param array $flux Flux d'entrée contenant la chaine affichée
 *
 * @return array Flux complété
 */
function galactic_contrib_affiche_droite(array $flux) : array {
	// Identification de la page et de l'objet
	$exec = $flux['args']['exec'];

	// Récupérer la liste des objets qui supporte une couleur
	include_spip('inc/config');
	$objets_config = lire_config('couleur_objet/objets', []);

	include_spip('inc/svptype_type_plugin');
	if (
		in_array('spip_mots', $objets_config) // si configuration objets ok
		and ($exec === 'type_plugin') // page d'un objet éditorial
		and ($id_objet = (int) ($flux['args']['id_mot']))
		and ($typologie = _request('typologie'))
		and ($typologie === 'categorie')
		and (type_plugin_lire($typologie, $id_objet, 'profondeur') == 0)
	) {
		$couleur = sql_getfetsel(
			'couleur_objet',
			'spip_couleur_objet_liens',
			[
				'objet=' . sql_quote('mot'),
				'id_objet=' . $id_objet
			]
		);
		$contexte = [
			'objet'         => 'mot',
			'id_objet'      => $id_objet,
			'couleur_objet' => $couleur
		];
		$flux['data'] .= recuperer_fond('inclure/couleur_objet', $contexte);
	}

	return $flux;
}

/**
 * Insertion dans le pipeline boite_infos.
 * - Fiche objet d'un plugin :
 *   - Rajouter un lien privé vers la rubrique associée si elle existe.
 *   - Enrichir le préfixe avec la couleur de la catégorie du plugin.
 *
 * @pipeline boite_infos
 *
 * @param array $flux Le contexte du pipeline
 *
 * @return array Le contexte du pipeline modifié
 */
function galactic_contrib_boite_infos(array $flux) : array {
	if (isset($flux['args']['type'])) {
		// Initialisation du type d'objet concerné.
		$objet = $flux['args']['type'];

		if (
			($objet_exec = trouver_objet_exec($objet))
			and !$objet_exec['edition']
			and ($objet === 'plugin')
			and ($id_plugin = (int) ($flux['args']['id']))
		) {
			// Page d'un plugin.

			// Ajout du bouton "voir la rubrique..."
			// -- On recherche le préfixe du plugin
			include_spip('inc/svp_plugin');
			$prefixe = plugin_lire($id_plugin, 'prefixe');

			// -- Inclure le bouton "voir la rubrique" si elle existe
			$contexte = [
				'id_plugin' => $id_plugin,
				'prefixe'   => $prefixe,
			];
			if ($bouton = recuperer_fond('prive/squelettes/inclure/inc-bouton_voir_rubrique_plugin', $contexte)) {
				$flux['data'] .= $bouton;
			}

			// Coloration du préfixe du plugin
			// -- on recherche la catégorie du plugin
			include_spip('inc/contrib_plugin');
			$categorie = plugin_lire_categorie($prefixe, 'racine');

			if ($categorie) {
				// -- on ajoute au span une class décrivant la couleur de la catégorie
				$cherche = "/(<p[^>]*class=(?:'|\")prefixe[^>]*>\s*)(<span>)/is";
				if (preg_match($cherche, $flux['data'])) {
					$flux['data'] = preg_replace(
						$cherche,
						'$1' . "<span class=\"couleur_{$categorie}\">",
						$flux['data'],
						1
					);
				}
			}
		}
	}

	return $flux;
}

/**
 * Modifier l'affichage du logo d'un objet.
 * - Pour les balises #LOGO_ARTICLE_RUBRIQUE et #LOGO_RUBRIQUE d'un plugin on force le logo du plugin.
 *
 * @pipeline quete_logo_objet
 *
 * @param array $flux Le contexte du pipeline
 *
 * @return array Le contexte du pipeline modifié
 */
function galactic_contrib_quete_logo_objet(array $flux) : array {
	if (
		isset($flux['args']['objet'])
		and ($objet = $flux['args']['objet'])
		and in_array($objet, ['rubrique', 'article'])
		and ($flux['args']['mode'] === 'on')
	) {
		// On est bien en train d'afficher le logo d'un article ou d'une rubrique
		$id_objet = (int) ($flux['args']['id_objet']);

		// On vérifie que l'objet est bien dans une rubrique plugin sinon on laisse le logo tel quel
		include_spip('action/editer_objet');
		if ($objet === 'article') {
			$id_rubrique = (int) objet_lire('article', $id_objet, ['champs' => 'id_rubrique']);
		} else {
			$id_rubrique = $id_objet;
		}
		if (
			include_spip('inc/contrib_rubrique')
			and rubrique_dans_secteur_plugin($id_rubrique)
			and ($prefixe = objet_lire('rubrique', $id_rubrique, ['champs' => 'prefixe']))
		) {
			// On est bien dans une rubrique de plugin, on peut forcer le logo dudit plugin
			// -- on récupère le logo du plugin connu par son préfixe
			include_spip('inc/contrib_plugin');
			if ($logo = plugin_lire_logo($prefixe)) {
				$flux['data'] = [
					'chemin'    => tester_url_absolue($logo) ? $logo : find_in_path($logo),
					'timestamp' => false,
					'fichier'   => ''
				];
			}
		}
	}

	return $flux;
}

function galactic_contrib_archivage_liste_motifs(array $flux) : array {
	// On rajoute des motifs pour Contrib
	$etat = $flux['args']['etat'];
	$objet = $flux['args']['objet'];
	$motifs_contrib = [
		"{$etat}_{$objet}_obsolescence"
	];
	$flux['data'] = array_merge($flux['data'], $motifs_contrib);

	return $flux;
}

function galactic_contrib_archivage_defaut_motif(array $flux) : array {
	// On rajoute des motifs pour Contrib
	$etat = $flux['args']['etat'];
	$objet = $flux['args']['objet'];
	$flux['data'] = "{$etat}_{$objet}_obsolescence";

	return $flux;
}
