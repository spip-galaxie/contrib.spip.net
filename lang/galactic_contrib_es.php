<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_contrib-contrib_spip_net?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// A
	'articles_top_notes' => 'Las preferidas',
	'articles_top_popularite' => 'Las más leidas',

	// C
	'contribution_sincrire' => 'commencer à contribuer', # MODIF

	// D
	'dernieres_modifs' => 'Últimas modificaciones', # MODIF
	'derniers_articles' => 'Las últimas',

	// I
	'info_auteurs' => 'Contribuidores',
	'info_rechercher' => '¡Encontrar!',
	'info_rechercher_02' => 'Buscar en este sitio',

	// M
	'ma_page' => 'Mi página',

	// P
	'par_contributions' => 'Principales contribuidores y contribuidoras',
	'par_nom' => 'Por Nombre',
	'par_note' => 'Con mejores calificaciones',
	'par_popularite' => 'Los más populares',
	'participation_auteur' => 'contribuyó en:',

	// T
	'telechargement' => 'Descargas',

	// V
	'versions' => 'Compatibilidad',

	// W
	'wiki' => 'Cuaderno wiki',
];
