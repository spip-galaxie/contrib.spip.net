<?php

/**
 * Retire les liens en syntaxe Trac: [URL titre] ---> titre
 *
 * @param string $texte Texte dans lequel chercher la syntaxe Trac
 *
 * @return array|mixed|string|string[]
 */
function tracbrut(string $texte) {
	if (preg_match_all('/[[][^ ]* *([^]]*)]/', $texte, $m, PREG_SET_ORDER)) {
		foreach ($m as $r) {
			$texte = str_replace($r[0], $r[1], $texte);
		}
	}

	return $texte ?: 'Log';
}

/**
 * Compile la balise `#ID_SECTEUR_AIDE` qui renvoie l'id du secteur qui matérialise l'aide de Contrib.
 * La signature de la balise est : `#ID_SECTEUR_AIDE`.
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ID_SECTEUR_AIDE_dist($p) {
	static $id = null;

	if (null === $id) {
		include_spip('inc/config');
		$id = lire_config('contrib/secteur_aide', 0);
	}
	$p->code = "{$id}";

	return $p;
}

/**
 * Compile la balise `#ID_SECTEUR_GALAXIE` qui renvoie l'id du secteur qui matérialise la rubrique Vie de SPIP.
 * La signature de la balise est : `#ID_SECTEUR_GALAXIE`.
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ID_SECTEUR_GALAXIE_dist($p) {
	static $id = null;

	if (null === $id) {
		include_spip('inc/config');
		$id = lire_config('contrib/secteur_galaxie', 0);
	}
	$p->code = "{$id}";

	return $p;
}

/**
 * Compile la balise `#ID_RUBRIQUE_CARNET` qui renvoie l'id du secteur qui matérialise le carnet wiki.
 * La signature de la balise est : `#ID_RUBRIQUE_CARNET`.
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ID_RUBRIQUE_CARNET_dist($p) {
	static $id = null;

	if (null === $id) {
		include_spip('inc/config');
		$id = lire_config('gribouille/secteur_wiki', 0);
		if (!$id) {
			$ids = lire_config('autorite/espace_wiki');
			if ($ids) {
				$id = reset($ids);
			}
		}
	}
	$p->code = "{$id}";

	return $p;
}

/**
 * Compile la balise `#IDS_SECTEUR_PLUGIN` qui renvoie les id des secteurs qui matérialisent des catégories de plugins.
 * La signature de la balise est : `#IDS_SECTEUR_PLUGIN`.
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_IDS_SECTEUR_PLUGIN_dist($p) {
	$p->code = "(include_spip('inc/contrib_rubrique')
		? rubrique_lister_secteur_plugin()
		: [])";

	return $p;
}

/**
 * @param $idb
 * @param $boucles
 * @param $crit
 *
 * @return void
 */
function critere_contrib_recherche_fulltext_dist($idb, &$boucles, $crit) {
	// sur contrib, la recherche sur le titre des forums est pas pertinente
	include_spip('galactic_fonctions');
	critere_recherche_fulltext_dist($idb, $boucles, $crit, ['forum' => ['titre']]);
}

/**
 * Reçoit une url et retourne l'article associé à cette url :
 * - soit l'article directement si c'est une url d'article
 * - soit l'article d'accueil de la rubrique si c'est une url de rubrique.
 *
 * @param string $url URL d'article ou de rubrique
 *
 * @return int Les id_articles
**/
function get_articles_from_url(string $url) : int {
	static $articles = [];

	if (!isset($articles[$url])) {
		if (preg_match(',^http(?:s)?://(?:www\.|spip-)?(?:spip-)?contrib(?:\.spip)?\.net/(\d+)$,', $url, $m)) {
			// L'url indique directement l'id de l'article
			$articles[$url] = (int) ($m[1]);
		} else {
			// Initialiser l'id article à 0 pour indiquer que l'url ne permet pas de trouver un article de contrib
			$articles[$url] = 0;

			[$fond, $contexte] = urls_decoder_url($url);
			// On considère qu'un lien de doc vers une rubrique équivaut à indiquer tous les articles, donc aucun
			// particulièrement
			if (
				($fond === 'article')
				and isset($contexte['id_article'])
			) {
				$articles[$url] = (int) ($contexte['id_article']);
			}
		}
	}

	return $articles[$url];
}

/**
 * A partir de l'url d'une source, renvoie l'url du gestionnaire de ticket.
 *
 * @param string $url URL de la source
 *
 * @return string L'url du gestionnaire de ticket ou vide sinon
**/
function get_tickets_from_source(string $url) : string {
	if (strpos($url, 'http') === false) {
		return '';
	}
	// Supprimer le .git éventuel à la fin de l'url
	$url = preg_replace('#\.git$#', '', $url);

	// Et ajouter /issues, cela marche pour gitlab, gitea, github
	return "{$url}/issues";
}
