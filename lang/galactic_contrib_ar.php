<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_contrib-contrib_spip_net?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// A
	'articles_top_notes' => 'الأعلى تقييماً',
	'articles_top_popularite' => 'الأكثر قراءة',

	// C
	'contribution_sincrire' => 'commencer à contribuer', # MODIF

	// D
	'date_maj' => 'به روز آوري ', # MODIF
	'dernieres_modifs' => 'أحدث التعديلات', # MODIF
	'derniers_articles' => 'مساهمات يا جماعة الخير!',

	// I
	'info_auteurs' => 'مساهمون',
	'info_rechercher' => 'عثرنا عليه!',
	'info_rechercher_02' => 'بحث في هذا الموقع',

	// M
	'ma_page' => 'صفحتي',

	// P
	'par_contributions' => 'المساهمون الأساسيون',
	'par_nom' => 'حسب الاسم',
	'par_note' => 'الأعلى تقييماً',
	'par_popularite' => 'الأكثر شعبية',
	'participation_auteur' => 'ساهم في:',

	// T
	'telechargement' => 'تحميل',
	'tickets' => 'التذاكر',

	// V
	'versions' => 'توافق',

	// W
	'wiki' => 'مدونة ويكي',
];
