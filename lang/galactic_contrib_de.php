<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_contrib-contrib_spip_net?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'contribution_sincrire' => 'commencer à contribuer', # MODIF

	// D
	'dernieres_modifs' => 'Letzte Änderungen', # MODIF
];
