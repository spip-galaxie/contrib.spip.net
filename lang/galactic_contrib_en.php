<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_contrib-contrib_spip_net?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// A
	'annees_depuis_nb' => 'For @nb@ years',
	'annees_depuis_un' => 'For 1 year',
	'annees_nb' => '@nb@ years',
	'annees_un' => '1 year',
	'article_forum' => 'Discussion',
	'articles_nb' => '@nb@ articles',
	'articles_top_notes' => 'Most rated',
	'articles_top_popularite' => 'Most read',
	'articles_un' => '1 article',
	'articles_zero' => '0 article',
	'autodoc' => 'Autodoc of plugins',
	'autres_telechargements' => 'Other downloads',

	// B
	'branche_x' => 'Branch @x@',

	// C
	'carnet' => 'Notebook',
	'classer_par_contributions' => 'Sort by contributions',
	'classer_par_date' => 'Sort by date',
	'classer_par_nom' => 'Sort by name',
	'classer_par_note' => 'Sort by note',
	'classer_par_pertinence' => 'Sort by relevance',
	'classer_par_popularite' => 'Sort by popularity',
	'code_source' => 'Source code',
	'commentaires_nb' => '@nb@ comments',
	'commentaires_un' => '1 comment',
	'commentaires_zero' => '0 comment',
	'contribution_sincrire' => 'commencer à contribuer', # MODIF

	// D
	'date_maj' => 'updated on',
	'depuis_origine' => 'Since the beginning',
	'dernieres_discussions_par_date' => 'Discussions by date of activity',
	'dernieres_modifs' => 'Latest modifications',
	'derniers_articles' => 'Keep going!',
	'discussions_nb' => '@nb@ discussions',
	'discussions_un' => 'One discussion',
	'discussions_zero' => 'No discussion',

	// I
	'info_auteurs' => 'Contributors',
	'info_filtrer' => 'Filter',
	'info_rechercher' => 'Found!',
	'info_rechercher_02' => 'Search in this site',
	'info_reponses_nb' => '@nb@ responses',
	'info_reponses_un' => '1 response',
	'info_reponses_zero' => '0 response',
	'info_votes_moins_nb' => '@nb@ negative votes',
	'info_votes_moins_un' => '1 negative vote',
	'info_votes_moins_zero' => '0 negative vote',
	'info_votes_nb' => '@nb@ votes',
	'info_votes_plus_nb' => '@nb@ positive votes',
	'info_votes_plus_un' => '1 positive vote',
	'info_votes_plus_zero' => '0 positive vote',
	'info_votes_un' => '1 vote',
	'info_votes_zero' => '0 vote',

	// L
	'liens_plugins_net' => 'In the plugins directory',

	// M
	'ma_page' => 'My page',
	'message_dans_discussion' => 'Message in the discussion',
	'message_suivant_dans_discussion' => 'Next message in the discussion',
	'messages_auteur' => 'commented: ',
	'mois_depuis_nb' => 'For @nb@ months',
	'mois_depuis_un' => 'For 1 month',
	'mois_nb' => '@nb@ months',
	'mois_un' => '1 month',

	// P
	'par_contributions' => 'Main contributors',
	'par_nom' => 'By Name',
	'par_note' => 'Most rated',
	'par_popularite' => 'Most popular',
	'participation_auteur' => 'contribute to:',
	'permalink' => 'Permanent link',
	'prefixe' => 'Prefix:',

	// R
	'rechercher_dans_articles' => 'Search in the articles',
	'rechercher_dans_carnet' => 'Search in the wiki notebook ',
	'rechercher_dans_commentaires' => 'Search in comments',
	'rechercher_dans_rubriques' => 'Search in sections',
	'repondre_article' => 'Add a comment',
	'rubriques_nb' => '@nb@ sections',
	'rubriques_un' => '1 section',
	'rubriques_zero' => '0 section',

	// S
	'sans_limite' => 'Without limit',

	// T
	'telechargement' => 'Download',
	'telechargements' => 'Downloads',
	'tickets' => 'Bug reports',

	// V
	'versions' => 'Compatibility',
	'voir_derniere_version' => 'See only the latest version by branch',
	'voir_toutes_versions' => 'See all versions',

	// W
	'wiki' => 'Wiki notepad',
];
